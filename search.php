<?php get_header(); ?>
<div class="content-wrapper <?php echo getWebsiteSection(); ?>">
    <section id="content" role="main" class="container content-container">
        <div class="left-shadow"></div>
        <div class="right-shadow"></div>
        <?php if ( have_posts() ) : ?>
            <header class="header row">
                <h1 class="entry-title col-sm-12"><?php printf( __( 'Search Results for: %s', 'dsbrown' ), get_search_query() ); ?></h1>
            </header>
            <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'entry' ); ?>
            <?php endwhile; ?>
            <?php get_template_part( 'nav', 'below' ); ?>
            <?php else : ?>
            <article id="post-0" class="post no-results not-found row">
                <header class="header col-sm-12">
                    <h2 class="entry-title"><?php _e( 'Nothing Found', 'dsbrown' ); ?></h2>
                </header>
                <section class="entry-content col-sm-12">
                    <p><?php _e( 'Sorry, nothing matched your search. Please try again.', 'dsbrown' ); ?></p>
                    <?php get_search_form(); ?>
                </section>
            </article>
            <?php endif; ?>
    </section>
<?php // get_sidebar(); ?>
<?php get_footer(); ?>
