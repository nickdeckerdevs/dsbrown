<?php
require_once( trailingslashit( get_stylesheet_directory() ) . 'Kint.class.php' );

add_action( 'after_setup_theme', 'dsbrown_setup' );
function dsbrown_setup() {
    load_theme_textdomain( 'dsbrown', get_template_directory() . '/languages' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'post-thumbnails' );
    global $content_width;
    if ( ! isset( $content_width ) )
        $content_width = 640;
    register_nav_menus(
        array( 'main-menu' => __( 'Main Menu', 'dsbrown' ) )
    );
}
add_action( 'wp_enqueue_scripts', 'dsbrown_load_scripts' );
function dsbrown_load_scripts() {
    wp_enqueue_script( 'jquery' );
}
add_action( 'comment_form_before', 'dsbrown_enqueue_comment_reply_script' );
function dsbrown_enqueue_comment_reply_script() {
    if ( get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_filter( 'the_title', 'dsbrown_title' );
function dsbrown_title( $title ) {
    if ( $title == '' ) {
        return '&rarr;';
    } else {
        return $title;
    }
}
add_filter( 'wp_title', 'dsbrown_filter_wp_title' );
function dsbrown_filter_wp_title( $title ) {
    return $title . esc_attr( get_bloginfo( 'name' ) );
}
add_action( 'widgets_init', 'dsbrown_widgets_init' );
function dsbrown_widgets_init() {
    register_sidebar( array (
        'name' => __( 'Sidebar Widget Area', 'dsbrown' ),
        'id' => 'primary-widget-area',
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => "</li>",
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
}
function dsbrown_custom_pings( $comment ) {
    $GLOBALS['comment'] = $comment;
    ?> <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
    <?php
}
add_filter( 'get_comments_number', 'dsbrown_comments_number' );
function dsbrown_comments_number( $count ) {
    if ( !is_admin() ) {
        global $id;
        $comments_by_type = &separate_comments( get_comments( 'status=approve&post_id=' . $id ) );
        return count( $comments_by_type['comment'] );
    } else {
        return $count;
    }
}

function content_func( $atts, $content = null ){
	$a = shortcode_atts( array(
		'class' => ''
	), $atts );
	$output = '</div><div class="' . ( ($a['class']) ? $a['class'] : '' ) . '">'
            . do_shortcode($content);
	return $output;
}
add_shortcode( 'content', 'content_func' );

function get_language() {
    $languages_not_english = [ 'es', 'fr', 'pt', 'zh' ];
    $url = $_SERVER['REQUEST_URI'];
	$split_url = explode('/', $url);
    $lang = in_array($split_url[1], $languages_not_english) ? $split_url[1] : 'en';
    return trim($lang);
}

remove_filter('the_content', 'wpautop');


function getWebsiteSection() {
     $firstSection = strtolower(explode('/', $_SERVER['REQUEST_URI'])[1]);
     switch($firstSection) {
         case 'es':
         case 'fr':
         case 'pt':
         case 'zh':
            return strtolower(explode('/', $_SERVER['REQUEST_URI'])[2]);
        default:
            return $firstSection;
     }
}

// add hook
add_filter( 'wp_nav_menu_objects', 'my_wp_nav_menu_objects_sub_menu', 10, 2 );

// filter_hook function to react on sub_menu flag
function my_wp_nav_menu_objects_sub_menu( $sorted_menu_items, $args ) {
  if ( isset( $args->sub_menu ) ) {
    $root_id = 0;

    // find the current menu item
    foreach ( $sorted_menu_items as $menu_item ) {
      if ( $menu_item->current ) {
        // set the root id based on whether the current menu item has a parent or not
        $root_id = ( $menu_item->menu_item_parent ) ? $menu_item->menu_item_parent : $menu_item->ID;
        break;
      }
    }

    // find the top level parent
    if ( ! isset( $args->direct_parent ) ) {
      $prev_root_id = $root_id;
      while ( $prev_root_id != 0 ) {
        foreach ( $sorted_menu_items as $menu_item ) {
          if ( $menu_item->ID == $prev_root_id ) {
            $prev_root_id = $menu_item->menu_item_parent;
            // don't set the root_id to 0 if we've reached the top of the menu
            if ( $prev_root_id != 0 ) $root_id = $menu_item->menu_item_parent;
            break;
          }
        }
      }
    }
    $menu_item_parents = array();
    foreach ( $sorted_menu_items as $key => $item ) {
      // init menu_item_parents
      if ( $item->ID == $root_id ) $menu_item_parents[] = $item->ID;
      if ( in_array( $item->menu_item_parent, $menu_item_parents ) ) {
        // part of sub-tree: keep!
        $menu_item_parents[] = $item->ID;
      } else if ( ! ( isset( $args->show_parent ) && in_array( $item->ID, $menu_item_parents ) ) ) {
        // not part of sub-tree: away with it!
        unset( $sorted_menu_items[$key] );
      }
    }
    return $sorted_menu_items;
  } else {
    return $sorted_menu_items;
  }
}

function top_menu_shortcode() {
    $menu =  wp_nav_menu( array(
      'menu' => 'primary-meny',
      'sub_menu' => true,
      'depth' => 1,
      'echo' => false,
      'after' => '<hr>',
      'direct_parent' => true
    ) );
    $menu .= '<div class="clear"></div>';
    return $menu;
}
add_shortcode('top-menu', 'top_menu_shortcode');

/* mail stuff from contact us page */

// phpinfo();


add_action( 'wp_ajax_submit_content', 'my_submission_processor' );
add_action( 'wp_ajax_nopriv_submit_content', 'my_submission_processor' );

function my_submission_processor() {
	$post = $_POST;
    $files = $_FILES;
    $emails = $post['emails'];
    // for testing purposes
    // $to = 'hollyce@threadgroup.com,nickdeckerdevs@gmail.com';
    $to = $emails;
    $name = ucwords($post['first']).' '.ucwords($post['last']);
    $subject = $name . ' <' . $post['email'] . '> ' . $post['about'].' Website Submission';
    $message = '<h2>Message from ' . $name . '</h2>';
    $message .= '<p>Comments: ' .$post['comments'];
    $message .= '<table width="650" cellpadding="5">';
    foreach($post as $key => $value) {
        switch($key) {
            case 'my_nonce_field':
            case '_wp_http_referer':
            case 'action':
            case 'additional-attachment':
            case 'fileurl':
            case 'bridge-product':
                break;
            case 'phone':
                if($value != '') {
                    $message .= "<tr><td>" . ucwords($key) . "</td><td><a href=\"tel:$value\">$value</a></td></tr>";
                }
                break;
                // for testing where the email was sent to.
            // case 'email':
            //     if($value != '') {
            //         $message .= "<tr><td>Sent To the following emails</td><td><a href=\"mailto:$value\">$value</a></td></tr>";
            //     }
            //     break;
            case 'product-nice-name':
                if($value != '') {
                    $message .= "<tr><td>Product</td><td>$value</td></tr>";
                }
                break;
            case 'about':
                if($value != '') {
                    $message .= "<tr><td>Inquiry Type</td><td>$value</td></tr>";
                }
                break;
            default:
            if($value != '') {
                $message .= "<tr><td>" . ucwords($key) . "</td><td>$value</td></tr>";
            }

        }
    }
    $message .= "</table>";
    $headers = array('Content-Type: text/html; charset=UTF-8');
    $response = '';
    if(strpos(',', $to) > -1) {
        $multiple = explode(',', $to);
        foreach($multiple as $single) {
            if(wp_mail( $single, $subject, $message, $headers )) {
                echo "<p style='clear:both; margin-top: 20px;'>Your Submittion has been sent. Thank you.</p>";
            } else {
                echo "<p style='clear:both; margin-top: 20px;'>Message failed please call 419.257.3561.</p>";
            }
        }
    } else {
        if(wp_mail( $to, $subject, $message, $headers )) {
            echo "<p style='clear:both; margin-top: 20px;'>Your Submittion has been sent. Thank you.</p>";
        } else {
            echo "<p style='clear:both; margin-top: 20px;'>Message failed please call 419.257.3561.</p>";
        }
    }
    die();
}

?>
