<?php get_header(); ?>
<div class="content-wrapper <?php echo getWebsiteSection(); ?>">
    <section id="content" role="main" class="container content-container">
        <div class="left-shadow"></div>
        <div class="right-shadow"></div>
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class('row'); ?>>
                <section class="entry-content col-md-12">
                    <?php the_content(); ?>
                </section>
            </article>
        <?php endwhile; endif; ?>
    </section>
</div>
<?php get_footer(); ?>
