
/* Gulp Variables */
const username = require('username');
var local = __dirname;
var directories = local.split('/');
var repo = directories[directories.length - 1];
var user = username.sync();
var scssIncludePaths = ['scss'];
var file = '--';

/* Node Dependencies */

var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var sassIncl = require('sass-include-paths');
var run = require('gulp-run');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var gutil = require('gulp-util');
var debug = require('gulp-debug');


gulp.task('default', ['watch']);

gulp.task('build', function() {
    var onError = function(err) {
        notify.onError({
            title: "Gulp",
            subtitle: "Error: ",
            message: "<%= error.message %>",
            sound: "Pop"
        })(err);
        this.emit('end');
    }
    return gulp.src(local+'/scss/main.scss')
        .pipe(plumber({errorHandler: onError}))
        .pipe(rename('/styles.scss'))
        .pipe(sass({
            includePaths: scssIncludePaths
        }))
        .pipe(gulp.dest(local));
});

gulp.task('deploy', function() {
    return gulp.src('./styles.css')
        .pipe(gulp.dest('css'));
});

gulp.task('watch', function() {
    var watcher = gulp.watch('**/**/*.scss', ['build', 'deploy']);
    watcher.on('change', function(e) {
        file = e.path.split('/');
        file = file.slice(-3).join('/');
        console.log('Building after file '+file+' was chanaged...');
    });
});
