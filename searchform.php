<?php
$language = get_language();
$search = [
    'en' => 'Search',
    'es' => 'Búsqueda',
    'fr' => 'Rechercher',
    'pt' => 'Busca',
    'zh' => '搜索'
];
?>
<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
    <label>
        <span class="screen-reader-text"><?php echo _x( $search[$language], 'label' ) ?></span>
        <input type="search" class="search-field"
            value="<?php echo get_search_query() ?>" name="s"
            title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
    </label>
    <input type="submit" class="search-submit" id="searchsubmit"
        value="Search" />
</form>
