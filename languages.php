<?php
$language = get_language();
$footer_headline = [
    'en' => 'BRIDGE THE WORLD WITH LEADING INFRASTRUCTURE SOLUTIONS.',
    'es' => 'TENDEMOS PUENTES AL MUNDO CON SOLUCIONES LIDERES EN INFRAESTRUCTURA',
    'fr' => 'NOUS RELIONS LE MONDE AVEC LES MEILLEURES SOLUTIONS D\'INFRASTRUCTURE',
    'pt' => 'Construindo pontes em todo o mundo com soluções líderes em infraestrutura',
    'zh' => '凭借领先的基础设施，架起沟通世界的桥梁。'
];
$footer_header = [
    'en' => 'COMPREHENSIVE SOLUTIONS',
    'es' => 'SOLUCIONES INTEGRALES',
    'fr' => 'SOLUTIONS COMPLÈTES',
    'pt' => 'SOLUÇÕES ABRANGENTES',
    'zh' => '全面型解决方案'
];
$footer_content = [
    'en' => 'D.S. Brown has the most comprehensive product-line serving the transportation industry. We manufacture solutions for the most challenging infrastructure applications. Our high quality engineered products are available worldwide for new construction and rehabilitation of bridges, highways, airfields, pavements, and parking structures. Please view our <a href="/Resources/DSBrown_Signature_Projectslowres.pdf" target="_blank">Signature Projects Portfolio</a>',
    'es' => 'DS Brown tiene la línea de productos más completa al servicio de la industria del transporte. Somos fabricantes de soluciones para las aplicaciones de infraestructura más difíciles. Nuestros productos de ingeniería de alta calidad están disponibles a nivel mundial para la nueva construcción y rehabilitación de puentes, carreteras, aeropuertos, pavimentos y estacionamientos.',
    'fr' => 'D.S. Brown propose au secteur de transport une famille complète de produits. Nous créons des solutions pour les infrastructures les plus difficiles. Nos produits fabriqués de haute qualité sont disponibles dans le monde entier pour les nouvelles constructions et la réhabilitation des ponts, des routes, des pistes d\'aéroport, des chaussées et des structures de stationnement.',
    'pt' => 'D.S. Brown possui a linha de produtos mais abrangente a serviço do setor de transportes. Fabricamos soluções para as aplicações mais exigentes em termos de infraestrutura. Nossos produtos de engenharia de alta qualidade estão disponíveis em todo o mundo para construções novas e projetos de reabilitação de pontes, rodovias, pistas de aeroportos e estruturas de estacionamento.',
    'zh' => 'DS Brown 公司为交通运输行业提供最丰富全面的产品系列。我们的解决方案可满足最具挑战的基础设施应用。质量上乘的工程产品适用于在全球范围内新建和翻修桥梁、高速公路、机场、道路和停车场结构。'
];
?>
