(function(e){var t=!1,i=!1,n={isUrl:function(e){var t=RegExp("^(https?:\\/\\/)?((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|((\\d{1,3}\\.){3}\\d{1,3}))(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*(\\?[;&a-z\\d%_.~+=-]*)?(\\#[-a-z\\d_]*)?$","i");return t.test(e)?!0:!1},loadContent:function(e,t){e.html(t)},addPrefix:function(e){var t=e.attr("id"),i=e.attr("class");"string"==typeof t&&""!==t&&e.attr("id",t.replace(/([A-Za-z0-9_.\-]+)/g,"sidr-id-$1")),"string"==typeof i&&""!==i&&"sidr-inner"!==i&&e.attr("class",i.replace(/([A-Za-z0-9_.\-]+)/g,"sidr-class-$1")),e.removeAttr("style")},execute:function(n,s,a){"function"==typeof s?(a=s,s="sidr"):s||(s="sidr");var r,d,l,c=e("#"+s),u=e(c.data("body")),f=e("html"),p=c.outerWidth(!0),g=c.data("speed"),h=c.data("side"),m=c.data("displace"),v=c.data("onOpen"),y=c.data("onClose"),x="sidr"===s?"sidr-open":"sidr-open "+s+"-open";if("open"===n||"toggle"===n&&!c.is(":visible")){if(c.is(":visible")||t)return;if(i!==!1)return o.close(i,function(){o.open(s)}),void 0;t=!0,"left"===h?(r={left:p+"px"},d={left:"0px"}):(r={right:p+"px"},d={right:"0px"}),u.is("body")&&(l=f.scrollTop(),f.css("overflow-x","hidden").scrollTop(l)),m?u.addClass("sidr-animating").css({width:u.width(),position:"absolute"}).animate(r,g,function(){e(this).addClass(x)}):setTimeout(function(){e(this).addClass(x)},g),c.css("display","block").animate(d,g,function(){t=!1,i=s,"function"==typeof a&&a(s),u.removeClass("sidr-animating")}),v()}else{if(!c.is(":visible")||t)return;t=!0,"left"===h?(r={left:0},d={left:"-"+p+"px"}):(r={right:0},d={right:"-"+p+"px"}),u.is("body")&&(l=f.scrollTop(),f.removeAttr("style").scrollTop(l)),u.addClass("sidr-animating").animate(r,g).removeClass(x),c.animate(d,g,function(){c.removeAttr("style").hide(),u.removeAttr("style"),e("html").removeAttr("style"),t=!1,i=!1,"function"==typeof a&&a(s),u.removeClass("sidr-animating")}),y()}}},o={open:function(e,t){n.execute("open",e,t)},close:function(e,t){n.execute("close",e,t)},toggle:function(e,t){n.execute("toggle",e,t)},toogle:function(e,t){n.execute("toggle",e,t)}};e.sidr=function(t){return o[t]?o[t].apply(this,Array.prototype.slice.call(arguments,1)):"function"!=typeof t&&"string"!=typeof t&&t?(e.error("Method "+t+" does not exist on jQuery.sidr"),void 0):o.toggle.apply(this,arguments)},e.fn.sidr=function(t){var i=e.extend({name:"sidr",speed:200,side:"left",source:null,renaming:!0,body:"body",displace:!0,onOpen:function(){},onClose:function(){}},t),s=i.name,a=e("#"+s);if(0===a.length&&(a=e("<div />").attr("id",s).appendTo(e("body"))),a.addClass("sidr").addClass(i.side).data({speed:i.speed,side:i.side,body:i.body,displace:i.displace,onOpen:i.onOpen,onClose:i.onClose}),"function"==typeof i.source){var r=i.source(s);n.loadContent(a,r)}else if("string"==typeof i.source&&n.isUrl(i.source))e.get(i.source,function(e){n.loadContent(a,e)});else if("string"==typeof i.source){var d="",l=i.source.split(",");if(e.each(l,function(t,i){d+='<div class="sidr-inner">'+e(i).html()+"</div>"}),i.renaming){var c=e("<div />").html(d);c.find("*").each(function(t,i){var o=e(i);n.addPrefix(o)}),d=c.html()}n.loadContent(a,d)}else null!==i.source&&e.error("Invalid Sidr Source");return this.each(function(){var t=e(this),i=t.data("sidr");i||(t.data("sidr",s),"ontouchstart"in document.documentElement?(t.bind("touchstart",function(e){e.originalEvent.touches[0],this.touched=e.timeStamp}),t.bind("touchend",function(e){var t=Math.abs(e.timeStamp-this.touched);200>t&&(e.preventDefault(),o.toggle(s))})):t.click(function(e){e.preventDefault(),o.toggle(s)}))})}})(jQuery);

function load_resource_sidebar_scripts() {
    jQuery('.resources-content h2:has(a+br), .resources-content h2:has(a+a)').each(function() {
        jQuery(this).find('a').css('font-weight', 700).css('font-size', '12px');
    });
    jQuery('.resources-content > div').each(function() {
        var clear = jQuery(this).css('clear');
        var height = jQuery(this).css('height');
        if(clear == 'both' && height == '20px') {
            jQuery(this).css('padding', 0);
        }
    });
    jQuery('.resources-content li:has(h3+h3)').each(function() {
        $this = jQuery(this);
        var text = $this.contents().get(0).nodeValue;
        var html = jQuery(this).html().split(text).join('<p style="margin-bottom:0;">'+text+'</p>');
        $this.html(html).addClass('resourcedownload');
    })
}
function all_movements(id) {
    switch(id) {
        case 'post-23':
            jQuery('.image-right').removeClass('half').removeClass('image-size').removeClass('image-right');
            break;
        case 'post-41':
            jQuery('.lg-content div').eq(3).find('img').eq(0).css('float', 'left').css('margin-top', '10px');
            jQuery('.lg-content div').eq(3).find('img').eq(1).css('float', 'right').css('margin-top', '10px');
            jQuery('.lg-content div').eq(3).find('br').remove();
            jQuery('.lg-content div').eq(3).find('p').remove();
            jQuery('.lg-content div').eq(3).find('h4').css('margin-top', '10px').css('float', 'left');
            break;
        case 'post-14':
        case 'post-2714':
            jQuery('.lg-content > div').eq(2).addClass('fixpage-14');
            jQuery('.lg-content > div').eq(3).addClass('fixpage-14');
            break;
        case 'post-91':
        case 'post-13':
        case 'post-89':
        case 'post-93':
            var pdf = '/wp-content/uploads/2017/04/DSBrown_Signature_Projectslowres.pdf';
            var link = '<a href="'+pdf+'" target="_blank">Signature Projects Portfolio</a>';
            var menu_item = '<li class="menu-item menu-item-type-post_type menu-item-object-page">'+link+'</a></li>';
            jQuery('.menu-primary-meny-container ul').append(menu_item);
            break;
        case 'post-9':
        case 'post-16':
            var lang = get_language();
            jQuery('a[href$="/employment"]').each(function() {
                jQuery(this).attr('href', lang+'/employment');
            });
        case 'post-18':
            var lang = get_language();
            jQuery('a[href$="/employment-application"]').each(function() {
                jQuery(this).attr('href', lang+'/employment-application');
            });
            break;
        case 'post-273':
            jQuery('.entry-content p').each(function() {
                $this = jQuery(this);
                if(!$this.hasClass('clear')) {
                    var content = $this.html();
                    if(content.indexOf('–') > -1) {
                        var content_array = content.split('–');
                        var html = '<p><strong>'+content_array[0]+'</strong> - '+content_array[1]+'</p>';
                        $this.html(html)
                    }
                }

            });
            jQuery('table tr').eq(4).find('td').css('line-height', '1px');
            jQuery('table tr').eq(8).find('td').css('line-height', '1px');
            jQuery('table tr').eq(12).find('td').css('line-height', '1px');
            jQuery('table tr').eq(16).find('td').css('line-height', '1px');
            jQuery('table tr').eq(22).find('td').css('line-height', '1px');
            jQuery('table tr').eq(26).find('td').css('line-height', '1px');
            jQuery('table tr').eq(30).find('td').css('line-height', '1px');
            jQuery('table tr').eq(34).find('td').css('line-height', '1px');
            jQuery('table tr').eq(38).find('td').css('line-height', '1px');
            jQuery('table tr').eq(42).find('td').css('line-height', '1px');
            jQuery('table tr').eq(46).find('td').css('line-height', '1px');
            jQuery('table tr').eq(50).find('td').css('line-height', '1px');
            jQuery('table tr').eq(56).find('td').css('line-height', '1px');
            jQuery('table tr').eq(60).find('td').css('line-height', '1px');
            jQuery('table tr').eq(64).find('td').css('line-height', '1px');
            jQuery('table tr').eq(68).find('td').css('line-height', '1px');
            jQuery('table tr').eq(74).find('td').css('line-height', '1px');
            jQuery('table tr').eq(78).find('td').css('line-height', '1px');
            jQuery('table tr').eq(82).find('td').css('line-height', '1px');
            jQuery('table tr').eq(86).find('td').css('line-height', '1px');
            jQuery('table tr').eq(92).find('td').css('line-height', '1px');
            jQuery('table tr').eq(96).find('td').css('line-height', '1px');
            jQuery('table tr').eq(100).find('td').css('line-height', '1px');
            break;


    }
}
function tablet_movements(id) {
    switch(id) {
        case 'post-26':
            jQuery('.md-content div').eq(1).css('float', 'left');
            jQuery('.md-content div').eq(3).css('float', 'left');
            break;
        case 'post-9':
            jQuery('.entry-content > div.col-md-12 > div').eq(4).css('margin-left', 0);
            break;

    }

}
function mobile_movements(id) {
    switch(id) {
        case 'post-38':
            var image = jQuery('.md-content > img');
            jQuery('.md-content > h2').before('<img src="'+image.attr('src')+'">');
            jQuery(image).remove();
            break;
        case 'post-34':
            jQuery('.md-content div').each(function() {
                switch(jQuery(this).css('float')) {
                    case 'left':
                    case 'right':
                        jQuery(this).addClass('fixphoneimage');
                        break;
                }
            });
            break;
        case 'post-24':
            var image = jQuery('.md-content > .image-right');
            jQuery('.md-content > h2').before('<img style="margin-top: 10px;" src="'+image.find('img').attr('src')+'">');
            jQuery(image).remove();
            break;
        case 'post-140':
            jQuery('object').after('<p style="font-style: italic;">Above is a Adobe Flash Video. If you are unable to see it, please visit page on desktop.</p>')
            break;
        case 'post-28':
        case 'post-29':
            var images = jQuery('.lg-content > div').eq(0);
            jQuery('.lg-content > p').eq(0).find('img').eq(0).before(images.html());
            jQuery(images).remove();
            break;
        case 'post-25':
            var benefits = jQuery('.entry-content > div').eq(1);
            jQuery('.entry-content > div').eq(0).find('h1').after('<div class="col-xs-12 benefits">'+benefits.html()+'</div>');
            jQuery(benefits).remove()
            break;
        case 'post-273':
            jQuery('.entry-content a:has(img)').addClass('imageholder');
            break;
    }
}

function get_language() {
    var language = $(location).prop('pathname').split('/')[1];
    switch(language) {
        case 'es':
        case 'fr':
        case 'pt':
        case 'zh':
            return '/'+language;
            break;

    }
    return '';

}

jQuery(window).load(function() {
    jQuery('#header #search .search-submit').val('');
    if(jQuery('body').hasClass('error404')) {
        jQuery('.entry-content > header.header + p').eq(0).remove();
        jQuery('#searchsubmit').val('Search')
    }
    jQuery('#menu-privacy li').eq(0).html(' | ' +jQuery('#menu-privacy li').eq(0).find('a').text());
    jQuery('#responsive-menu-button').sidr({
        name: 'sidr-main',
        source: '#mobile-menu',
        side: 'right'
    });
    jQuery.sidr('close','sidr-main');
    jQuery('.sidr-class-sidr-close').click(function() {
        jQuery.sidr('close','sidr-main');
        return false;
    });
    jQuery('.sidr-class-menu-item-has-children').each(function() {
        jQuery(this).prepend('<span class="sidr-dropdown" alt="open dropdown">v</span>');
    })
    jQuery('.sidr').on('click', '.sidr-dropdown', function(e) {
        e.stopImmediatePropagation();
        jQuery(this).parent().find('> ul').toggle();
    });
    var body = jQuery('body');

    /* None of these scripts load on the home page */

    if(!jQuery('body').hasClass('home')) {
        var device_width = jQuery(window).width();
        var article_id = jQuery('article').attr('id');

        var content_div = jQuery('.entry-content > div').eq(0).attr('class');
        var total_divs = jQuery('.entry-content > .'+content_div+' > div').length;
        jQuery('.entry-content > .'+content_div+' > div').each(function(index) {
            if (index === total_divs - 1) {
                if(!jQuery(this).hasClass('clear')) {
                    jQuery(this).after('<div class="clear"></div>');
                }
            }
        });
        /* through all images in content section */
        jQuery('.entry-content img').each(function() {
            var imagename = jQuery(this).attr('src');
            if(imagename.indexOf('WebButton_WithCart_Green') > -1) {
                jQuery(this).css('border', 'none');
            }
        });
        /* remove all the messed up spacing caused by WP */
        jQuery(".entry-content").each(function() {
            var $this = jQuery(this);
            $this.html($this.html().replace(/&nbsp;/g, ''));
        });
        /* this is mobile movements */

        if(device_width < 400) {
            mobile_movements(article_id);

        }
        if(device_width < 769) {
            tablet_movements(article_id);
        }
        all_movements(article_id);


    }
    /* resource section is present, so load these scripts */
    if(jQuery('.resources-content').length < -1) {
        load_resource_sidebar_scripts();
    }
});
