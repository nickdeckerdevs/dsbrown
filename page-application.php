<?php
/*
Template Name: Employment Application New
*/
get_header(); ?>
<div class="content-wrapper employment-application">
    <section id="content" role="main" class="container content-container">
        <div class="left-shadow"></div>
        <div class="right-shadow"></div>
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class('row'); ?>>
                <section class="entry-content col-md-12">
                    <div class="col-sm-6">
                        <h1><?php echo apply_filters( 'the_title', $post->post_title ); ?></h1>
                        <?php echo do_shortcode('[contact-form-7 id="2723" title="Employment Application"]'); ?>
                    </div>
                    <div class="col-md-4 benefits">
                        <?php the_content(); ?>
                    </div>
                </section>
            </article>
        <?php endwhile; endif; ?>
    </section>
</div>
<?php get_footer(); ?>

<!---
<div id="first-name"><label>First Name:</label>
    [text* first-name]
</div>
<div id="last-name"><label>Last Name:</label>
    [text* last-name]
</div>
<div id="address"><label>Address:</label>
    [text address]
</div>
<div id="city"><label>City:</label>
    [text city]
</div>
<div id="state"><label>State:</label>
    [text state]
</div>
<div id="zip"><label>ZIP:</label>
    [text zip]
</div>
<div id="country"><label>Country:</label>
    [text country]
</div>
<div id="phone"><label>Phone:</label>
    [text phone]
</div>
<div id="email"><label>Email:</label>
    [text* email]
</div>
<div id="discipline"><label>Select your Discipline:</label>
    [select select-discipline id:select-discipline multiple "Engineering" "Estimating" "International" "Operations" "Sales/Marketing"]
</div>
<div id="status"><label>Status:</label>
    [select select-status id:select-statis multiple "Full-Time" "Part-Time" "Contract/Temporary"]
</div>
<div id="relocate"><label>Willing to Relocate?:</label>
[radio radio-169 label_first use_label_element default:1 "Yes" "No"]
</div>
<div id="resume"><label>Resume (2MB Max):</label>
    [file file-320 limit:3000000 filetypes:pdf|doc|docx|rtf|txt]
</div>

<div id="your-message"><label>Additional Information:</label>
    [textarea your-message]
</div>
<div id="submit-container">
[submit "Send"]
</div>
--->





<?php
/*
Template Name: Employment Application
*/
get_header(); ?>
<div class="content-wrapper employment-application">
    <section id="content" role="main" class="container content-container">
        <div class="left-shadow"></div>
        <div class="right-shadow"></div>
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class('row'); ?>>
                <section class="entry-content col-md-12">
                    <div class="col-md-6">
                        <h1><?php echo apply_filters( 'the_title', $post->post_title ); ?></h1>
                        <?php echo do_shortcode('[contact-form-7 id="2723" title="Employment Application"]'); ?>
                    </div>
                    <div class="col-md-4 benefits">
                        <?php the_content(); ?>
                    </div>
                </section>
            </article>
        <?php endwhile; endif; ?>
    </section>
</div>
<?php get_footer(); ?>

<!---
<div id="first-name"><label>First Name:</label>
    [text* first-name]
</div>
<div id="last-name"><label>Last Name:</label>
    [text* last-name]
</div>
<div id="address"><label>Address:</label>
    [text address]
</div>
<div id="city"><label>City:</label>
    [text city]
</div>
<div id="state"><label>State:</label>
    [text state]
</div>
<div id="zip"><label>ZIP:</label>
    [text zip]
</div>
<div id="country"><label>Country:</label>
    [text country]
</div>
<div id="phone"><label>Phone:</label>
    [text phone]
</div>
<div id="email"><label>Email:</label>
    [text* email]
</div>
<div id="discipline"><label>Select your Discipline:</label>
    [select select-discipline id:select-discipline multiple "Engineering" "Estimating" "International" "Operations" "Sales/Marketing"]
</div>
<div id="status"><label>Status:</label>
    [select select-status id:select-statis multiple "Full-Time" "Part-Time" "Contract/Temporary"]
</div>
<div id="relocate"><label>Willing to Relocate?:</label>
[radio radio-169 label_first use_label_element default:1 "Yes" "No"]
</div>
<div id="resume"><label>Resume (2MB Max):</label>
    [file file-320 limit:3000000 filetypes:pdf|doc|docx|rtf|txt]
</div>

<div id="your-message"><label>Additional Information:</label>
    [textarea your-message]
</div>
<div id="submit-container">
[submit "Send"]
</div>
--->
