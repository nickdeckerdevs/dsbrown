<?php
/*
Template Name: Go to Maurer
*/
$lang = get_language();
$url = '';
switch($lang) {
    case 'es':
    case 'pt':
    case 'fr':
    case 'zh':
        $url = "/$lang/bridges/expansion-joint-systems/swivel/";
        break;
    default:
        $url = "/bridges/expansion-joint-systems/swivel/";
}
header("Location: $url");
?>
