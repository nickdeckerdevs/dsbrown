<?php
/*
Template Name: Contact Us
*/ ?>

<?php


$language = get_language();
$fieldText = [
    'en' => [
        'about' => 'Inquiry type',
        'product' => 'Select A Product',
        'first' => 'First Name',
        'last' => 'Last Name',
        'company' => 'Company',
        'country' => 'Country',
        'address' => 'Street Address',
        'city' => 'City',
        'state' => 'State',
        'zip' => 'Zip',
        'email' => 'Email',
        'phone'  => 'Phone',
        'comments' => 'Questions, Requests, or Comments',
        'file-attachment'  => 'Attachment (10MB Max.)',
        'submit' => 'Send Email'
    ],
    'es' => [
        'about' => 'Inquiry type',
        'product' => 'Select A Product',
        'first' => 'First Name',
        'last' => 'Last Name',
        'company' => 'Company',
        'country' => 'Country',
        'address' => 'Street Address',
        'city' => 'City',
        'state' => 'State',
        'zip' => 'Zip',
        'email' => 'Email',
        'phone'  => 'Phone',
        'comments' => 'Questions, Requests, or Comments',
        'file-attachment'  => 'Attachment (10MB Max.)',
        'submit' => 'Send Email'
    ],
    'fr' => [
        'about' => 'Diriger ce courrier électronique à',
        'product' => 'Select Product',
        'first' => 'Prénom',
        'last' => 'Nom de famille',
        'company' => 'Nom de l\'entreprise',
        'country' => 'Pays',
        'address' => 'Adresse postale',
        'city' => 'Ville',
        'state' => 'Province / région',
        'zip' => 'Code postal',
        'email' => 'Courrier électronique',
        'phone' => 'Téléphone',
        'comments' => 'Questions, requêtes ou commentaire',
        'file-attachment' => 'Pièce jointe (10MB Max.)',
        'submit' => 'Envoyer un message'

    ],
    'pt' => [
        'about' => 'Encaminhe este e-mail a',
        'product' => 'Select Product',
        'first' => 'Nome',
        'last' => 'Sobrenome',
        'company' => 'Empresa',
        'country' => 'País',
        'address' => 'Endereço',
        'city' => 'Cidade',
        'state' => 'Estado',
        'zip' => 'Código Postal	',
        'email' => 'Email',
        'phone' => 'Fone',
        'comments' => 'Perguntas, Pedidos ou Comentários',
        'file-attachment' => 'Anexo (Máximo de 10MB)',
        'submit' => 'Envie um e-mail'
    ],
    'zh' => [
        'about' => '请将邮件发送至',
        'product' => 'Select Product',
        'first' => '名字',
        'last' => '姓氏',
        'company' => '公司',
        'country' => '国家',
        'address' => '详细地址',
        'city' => '城市',
        'state' => '州',
        'zip' => '邮编',
        'email' => '电子邮箱',
        'phone' => '电话',
        'comments' => '疑问、请求或评论',
        'file-attachment' => '附件 (10MB 最大)',
        'submit' => '发送电邮'
    ]
];



$inquryOptions = '';
$productOptions = '';
$countryOptions = '';
$statesArray = [];
$products = [];
$reps = [];
$contactMapping = [];
function getCountryOptions($xml) {
    $options = '';
    foreach($xml as $node) {
        $value = $node['value'];
        $country = $node->__toString();
        $options .= "<option value=\"$value\">$country</option>";
    }
    return $options;
}

function getProductOptions($xml) {
    $options = '';
    foreach($xml as $node) {
        $value = $node['value']->__toString();
        $product = $node->__toString();
        $options .= "<option value=\"$value\">$product</option>";
    }
    return $options;
}
function getInquiryOptions($xml) {
    $options = '';
    foreach($xml as $node) {
        $inquiry = $node['name']->__toString();
        $options .= "<option value=\"$inquiry\">$inquiry</option>";
    }
    return $options;
}

function getContactMapping($xml) {
    $contact = [];
    foreach($xml as $node) {
        $category = $node['name']->__toString();
        $email = $node->value->__toString();
        $contact[$category] = $email;
    }
    return $contact;
}

function getStatesArray($xml) {
    $country = [];
    foreach($xml as $node) {
        $id = $node['id']->__toString();
        $country[$id] = '';
        foreach($node as $option) {
            $value = $option['value'];
            $state = $option->__toString();
            $country[$id] .= "<option value=\"$value\">$state</option>";
        }
    }
    return $country;
}

function getRepArray($node) {
    $attributes = $node->attributes();
    $regions = [];
    foreach($attributes as $index => $value) {
        $region_id = $value->__toString();
        array_push($regions, $region_id);

    }
    $rep = [];
    $rep['region_id'] = $regions;
    $rep['name'] = $node->name->__toString();
    $rep['phone'] = $node->phone->__toString();
    $rep['fax'] = $node->fax->__toString();
    $rep['email'] = $node->email->__toString();
    $rep['address'] = $node->address->__toString();
    $rep['picture'] = $node->picture->__toString();
    $rep['title'] = $node->title->__toString();
    // $rep['region'] =
    return $rep;
}

function getSalesPersonData($xml) {
    $product = new StdClass();
    $product->name = $xml['id']->__toString();
    $product->countries = [];
    foreach($xml->country as $node) {
        $country = $node['id']->__toString();
        $product->countries[$country] = [];
        $states = getStatesFromRegion($node->region);
        foreach($states as $state => $rep) {
            $product->countries[$country][$state] = $rep;
        }
    }
    // $product->region = [];
    // foreach($xml->country as $node) {
    //     $country = $node['id']->__toString();
    //     $product->countries[$country] = [];
    //     $states = getStatesFromRegion($node->region);
    //     foreach($states as $state => $rep) {
    //         $product->countries[$country][$state] = $rep;
    //     }
    // }
    return $product;
}

function getStatesFromRegion($regions) {
    foreach($regions as $region) {
        $stateString = $region['states']->__toString();
        if(strpos($stateString, ',') > -1) {
            $states = explode(',', $stateString);

            foreach($states as $state) {
                $stateArr[$state]  = $region['reps']->__toString();
            }
        } else {
            $stateArr[$stateString] = $region['reps']->__toString();
        }
    }
    return $stateArr;
}

if (file_exists( get_template_directory() . '/SalesRep.xml')) {
    $xml = simplexml_load_file( get_template_directory() . '/SalesRep.xml');
    foreach($xml as $node) {
        $name = $node->getName();
        $id = $node['id']->__toString();
        switch($name) {
            case 'country':
                $countryOptions = getCountryOptions($node);
            break;
            case 'product':
                if($id == 'productsSelect') {
                    $productOptions = getProductOptions($node);
                } else {
                    $products[$id] = getSalesPersonData($node);
                    // ddd($products);


                }
                break;
            case 'contact':
                $inquryOptions = getInquiryOptions($node);
                $contactMapping = getContactMapping($node);
                break;
            case 'states':
                $statesArray = getStatesArray($node);
                break;
            case 'rep':
                $repArray[$id] = getRepArray($node);
            default:

        }
    }

} else {
    exit('Failed to open test.xml.');
}
?>

<?php get_header(); ?>
<div class="content-wrapper <?php echo getWebsiteSection(); ?>">
    <section id="content" role="main" class="container content-container">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class('row'); ?>>
                <section class="entry-content col-md-12">
                    <div class="contact-page-content">
                        <?php the_content(); ?>
                    </div>
                    <div id="sales-rep-container">
                        <h1>Sales Representative Locator</h1>
                        <div id="sales-rep-content">
                            <div class="sales-rep-country">
                                <label>
                                    Select a Country:
                                </label>
                                <select name="sales-rep-country" id="sales-rep-country" class="required" required>
                                    <?php echo $countryOptions; ?>
                                </select>
                            </div>
                            <div class="sales-rep-state" style="visibility: hidden">
                                <label for="state">
                                    <?php echo $fieldText[$language]['state']; ?>
                                </label>
                                <select id="sales-rep-state" name="sales-rep-state">

                                </select>
                            </div>
                            <div class="sales-rep-product" style="visibility: hidden">
                                <label for="product">
                                    <?php echo $fieldText[$language]['product']; ?>
                                </label>
                                <select name="sales-rep-product" id="sales-rep-product">
                                     <?php echo $productOptions; ?>
                               </select>

                             </div>
                         </div>
                     </div>
                     <div id="contact-us-container" class="form-container col-md-12">
                        <form id="contact-us-form" action="" method="post" enctype="multipart/form-data">
                            <?php wp_nonce_field( 'submit_content', 'my_nonce_field' ); ?>
                            <div class="row">
                                <div class="form-field col-sm-6">
                                    <label for="about">
                                        * <?php echo $fieldText[$language]['about']; ?>
                                    </label>
                                    <?php
                                    $pt_about = '<option value="Customer Service">Serviços de atendimento ao cliente</option>
                                    <option value="Engineering">Engenharia</option>
                                    <option value="Estimating">Cotação</option>
                                    <option value="Human Resources">Recursos humanos</option>
                                    <option value="International-Asia/Africa/Australia Office">Internacional – escritório da Ásia/África/Austrália</option>
                                    <option value="International-North,South America/Europe">Internacional – América do Norte, do Sul/Europa</option>
                                    <option value="Marketing">Marketing</option>
                                    <option value="Operations">Operações</option>
                                    <option value="Product Installation Services">Serviços de instalação de produtos</option>
                                    <option value="Sales-Bridge Products">Vendas – Produtos para pontes</option>
                                    <option value="Sales-Pavement Products">Vendas – Produtos para pavimentos</option>
                                    <option value="General/Main">Geral/Principal</option>';
                                    $zh_about = '<option value="Customer Service">客户服务</option>
                                    <option value="Engineering">工程</option>
                                    <option value="Estimating">预算</option>
                                    <option value="Human Resources">人力资源</option>
                                    <option value="International-Asia/Africa/Australia Office">国际 — 亚洲/非洲/澳大利亚办事处</option>
                                    <option value="International-North,South America/Europe">国际 — 北美洲、南美洲/欧洲</option>
                                    <option value="Marketing">营销</option>
                                    <option value="Operations">运营</option>
                                    <option value="Product Installation Services">产品安装服务</option>
                                    <option value="Sales-Bridge Products">销售 — 桥梁产品</option>
                                    <option value="Sales-Pavement Products">销售 — 人行道产品</option>
                                    <option value="General/Main">一般/主要产品</option>';


                                    ?>
                                    <select name="about" id="about" class="required" required>
                                        <?php switch($language) {
                                            case 'en':
                                            case 'es':
                                            case 'fr':
                                                echo $inquryOptions;
                                                break;
                                            case 'pt':
                                                echo $pt_about;
                                                break;
                                            case 'zh':
                                                echo $zh_about;
                                                break;
                                            default:
                                                echo $inquryOptions;
                                            }
                                                ?>
                                   </select>

                               </div>

                               <div id="bridge-dropdown" class="form-field col-sm-6" style="visibility: hidden">
                                   <label for="bridges">
                                       <?php echo $fieldText[$language]['product']; ?>
                                   </label>
                                   <select name="bridge-product" id="bridge-product">
                                      	<?php echo $productOptions; ?>
                                  </select>

                                </div>
                            </div>
                            <div class="row">
                                <div class="form-field col-sm-6">
                                    <label for="first">
                                        * <?php echo $fieldText[$language]['first']; ?>
                                    </label>
                                    <input type="text" name="first" id="first" class="required" required>

                                </div>
                                <div class="form-field col-sm-6">
                                    <label for="last">
                                        * <?php echo $fieldText[$language]['last']; ?>
                                    </label>
                                    <input type="text" name="last" id="last" class="required" required>

                                </div>
                            </div>
                            <div class="row">

                                <div class="form-field col-sm-6">
                                    <label for="company">
                                        <?php echo $fieldText[$language]['company']; ?>
                                    </label>
                                    <input type="text" name="company" id="company">
                                </div>
                                <div class="form-field col-sm-6">
                                    <label for="country">
                                        * <?php echo $fieldText[$language]['country']; ?>
                                    </label>
                                    <select name="country" id="country" class="required" required>
                                        <?php echo $countryOptions; ?>
                                    </select>

                                </div>
                            </div>
                            <div class="row">
                                <div class="form-field col-sm-6">
                                    <label for="street">
                                        <?php echo $fieldText[$language]['address']; ?>
                                    </label>
                                    <input type="text" name="street" id="street">

                                </div>
                                <div class="form-field col-sm-6">
                                    <label for="city">
                                        * <?php echo $fieldText[$language]['city']; ?>
                                    </label>
                                    <input type="text" name="city" id="city" class="required" required>

                                </div>
                            </div>
                            <div class="row">

                                <div id="state-dropdown" class="form-field col-sm-6" >
                                    <label for="state">
                                        <?php echo $fieldText[$language]['state']; ?>
                                    </label>
                                    <select id="state" name="state">

                                    </select>

                                </div>
                                <div class="form-field col-sm-6">
                                    <label for="zip">
                                        <?php echo $fieldText[$language]['zip']; ?>
                                    </label>
                                    <input type="text" name="zip" id="zip">

                                </div>
                            </div>
                            <div class="row">
                                <div class="form-field col-sm-6">
                                    <label for="email">
                                        * <?php echo $fieldText[$language]['email']; ?>
                                    </label>
                                    <input type="email" name="email" id="email" class="required" required>

                                </div>
                                <div class="form-field col-sm-6">
                                    <label for="phone">
                                        <?php echo $fieldText[$language]['phone']; ?>
                                    </label>
                                    <input type="phone" name="phone" id="phone">

                                </div>
                            </div>
                            <div class="row">
                                <div class="form-field col-sm-6 attachment-container">
                                    <label for="attachment">
                                        <?php echo $fieldText[$language]['file-attachment']; ?>
                                    </label>
                                    <input type="file" name="additional-attachment" size="40" class="wpcf7-form-control wpcf7-file additional-attachment" id="additional-attachment" aria-invalid="false">
                                    <style>
                                        #upload-attachment-form-container { clear: both; display: none!important; }
                                    </style>
                                </div>
                                <div class="form-field col-sm-6">
                                    <label for="comments">
                                        <?php echo $fieldText[$language]['comments']; ?>
                                    </label>
                                    <textarea id="comments" name="comments" class="required"></textarea>

                                </div>
                            </div>
                            <div class="col-md-12 submit-container">
                                <input type="hidden" name="action" value="submit_content">
                                <input type="hidden" id="emails" name="emails" value="">
                                <input type="hidden" id="fileurl" name="fileurl" value="">
                                <input type="submit" name="submit" value="<?php echo $fieldText[$language]['submit']; ?>" id="submit">
                            </div>
                        </form>
                        <div id="upload-attachment-form-container">
                            <?php echo do_shortcode('[contact-form-7 id="328" title="File Attachment - Contact Us"]'); ?>
                        </div>


                </section>
            </article>
        <?php endwhile; endif; ?>
    </section>
</div>
<?php get_footer(); ?>
<div id="loading-background" style="display: none;">

    <div class="spinner">
        <h2>Loading</h2>
      <div class="rect1"></div>
      <div class="rect2"></div>
      <div class="rect3"></div>
      <div class="rect4"></div>
      <div class="rect5"></div>
    </div>
</div>

<script>
jQuery(window).load(function() {

    var fileUrl = '';
    var states = <?php echo json_encode($statesArray); ?>;
    var reps = <?php echo json_encode($repArray); ?>;
    var products = <?php echo json_encode($products); ?>;
    var aboutMapping = <?php echo json_encode($contactMapping); ?>;
    var displaySalesRep = false;
    jQuery('#country').val('US');
    jQuery('#state').html(states.US);
    jQuery('#state').attr('required', true);
    jQuery('#country').val('US');

    /* checks inquiry and product matrix for an email contact */
    function getTargetEmails() {
        var salesreps = [];
        var emails = [];
        var product = jQuery('#bridge-product').val();
        var about = jQuery('#about').val();
        if(product) {
            salesreps = getProductEmails(product);
            for(var i = 0; i < salesreps.length; i++) {
                emails.push(salesreps[i]);
            }
        }
        if(about) {
            var email = aboutMapping[about];
            if(jQuery.inArray(email, emails) == -1) {  //prevents duplicate email addresses
                emails.push(email);
            }
        }
        jQuery('#emails').val(emails);
        return emails;
    }

    /* gets emails addresses that go with the country / region / state */
    function getProductEmails(product) {
        var emails = [];
        var country = jQuery('#country').val();
        var state = jQuery('#state').val() ? jQuery('#state').val() : country;
        var countryRep = getCountryRep(product, country, state);
        for(var i = 0; i < countryRep.length; i++) {
            var abbrv = countryRep[i];
            if(abbrv) { // removes blank values
                var email = reps[abbrv].email;
                if(jQuery.inArray(email, emails) == -1) {  //prevents duplicate email addresses
                    emails.push(reps[abbrv].email);
                }
            }
        }
        return emails;
    }

    /*
        gets the reps for product -- as an array. There were some hangups
        data so we did some flopping of strings and arrays
    */
    function getCountryRep(product, country, state) {
        var rep = [];
        if(country.indexOf('ITL') > -1 || country.indexOf('ITZ') > -1) {
            var newCountry = products[product]['countries'][country.substring(0,3)];
            rep.push(newCountry[state]);
            rep.push(newCountry[state.substring(0,3)]);
            var repString = rep.join();
            rep = repString.split(',');

        } else {
            var newCountry = products[product]['countries'][country];
            var repString = newCountry[state];
            if(repString.indexOf(',') > -1) {
                rep = repString.split(',');
            } else {
                rep.push(repString);
            }
        }
        displaySalesRep = rep;
        return rep;
    }

    function triggerUpload() {
        jQuery('#attach-the-file').trigger('click');


    }



    function formIsValid() {
        var cForm = jQuery('#contact-us-form')[0];
        if(cForm.checkValidity()) {
            return true;
        }
        loading('off');
        return false;
    }

    function displaySalespersonInfo(rep) {
        var message = '';
        for(var i = 0; i < rep.length; i++) {
            var person = reps[rep[i]];
            message += '<div class="col-md-6 business-card">';
            message += '<div class="col-sm-6" style="margin-top: 3px;">';
            message += '<p><strong>' + person.name + '</strong></p>';
            message += '<p>' + person.title + '</p>';
            message += '<p>phone: <a href="tel:' + person.phone + '">' + person.phone + '</a></p>';
            message += '<p>fax: ' + person.fax + '</p>';
            message += '<p><a href="mailto:' + person.email + '">' + person.email + '</a></p>';
            message += '<p>' + person.address + '</p></div>';
            message += '<div class="col-sm-6" style="padding-right: 0;"><img style="float:right;" src="/wp-content/uploads/2017/04/' + person.picture + '">';
            message += '</div><div class="clear"></div></div>';
        }
        return message;
    }

    function sendEmail() {
        var productNiceName = jQuery("#bridge-product option:selected").text()
        if(productNiceName != '') {
            jQuery('#contact-us-form #about').after('<input type="hidden" name="product-nice-name" value="'+productNiceName+'">');
        }
        var newForm = jQuery('#contact-us-form').serialize();
        jQuery.ajax({
            type:"POST",
            url: "/wp-admin/admin-ajax.php",
            data: newForm,
            success:function(data){
                loading('off');
                var message = '<h1>Your submission has been sent. Thank you!</h1>';
                if(displaySalesRep) {

                    message += displaySalespersonInfo(displaySalesRep);

                }
                jQuery('.form-container').html(message + data);
            }
        });
    }
    function loading($toggle) {
        if($toggle == 'on') {
            jQuery('#loading-background').css('display', 'block').show();
        } else {
            jQuery('#loading-background').css('display', 'none').hide();
        }
    }

    // clones the upload into the wordpress contact form
    jQuery("#additional-attachment").change(function(){
        var $this = jQuery(this),
        $clone = $this.clone();
        $this.after($clone).appendTo(jQuery('.wpcf7-form'));
    });

    // listens for file upload to finish before sending form email
    var wpcf7Elm = document.querySelector( '.wpcf7' );
    wpcf7Elm.addEventListener( 'wpcf7submit', function( event ) {
        jQuery('#upload-attachment-form-container').hide();
        sendEmail();
    }, false );

    String.prototype.ucfirst = function() {
        return this.charAt(0).toUpperCase() + this.substr(1);
    }
    jQuery('#submit').on('click', function(e) {
        loading('on');
        if(formIsValid()) {
            e.preventDefault();
            // ga('send', 'event', 'Contact Form', 'Form', 'Contact DS Brown');
            emails = getTargetEmails();
            jQuery('#emails').val(emails)
            var fileupload = jQuery('#additional-attachment').val();
            jQuery('.attachment-container').remove();
            jQuery('#upload-attachment-form-container').hide();
            if(fileupload != '') {
                var emailname = jQuery('#first').val().ucfirst() + ' ' + jQuery('#last').val().ucfirst();
                jQuery('#email-name').val(emailname);
                var emailfrom = jQuery('#email').val();
                jQuery('#email-attachment').val(emailfrom);
                jQuery('#email-to').val(emails);
                jQuery('#attach-the-file').trigger('click');
            } else {
                sendEmail();
            }

        }
    });

    /* watches inquiry/about for changes to bring up product dropdown */
    jQuery('#about').on('change', function() {
        var value = jQuery(this).val();
        switch(value) {
            case 'Sales-Bridge Products':
                jQuery('#bridge-dropdown').css('visibility', 'visible');
                jQuery('#bridge-dropdown').css('display', 'block');
                jQuery('#bridge-product').attr('required', true);
                jQuery('#bridge-product').val('');
                break;
            case 'Sales-Pavement Products':
                jQuery('#bridge-dropdown').css('visibility', 'visible');
                jQuery('#bridge-dropdown').css('display', 'block');
                jQuery('#bridge-product').attr('required', true);
                jQuery('#bridge-product').val('Pavements1');
                break;
            default:
                jQuery('#bridge-dropdown').css('visibility', 'hidden');
                jQuery('#bridge-dropdown').css('display', 'none');
                jQuery('#bridge-product').attr('required', false)
        }
    });



    jQuery('.required').on('click', function() {
        var elem = jQuery(this);
        elem.parent().find('.message').hide()
    });


    jQuery('#sales-rep-button').on('click', function(e) {
        remove_business_card();
        jQuery('#contact-details').hide();
        jQuery('#contact-us-container').hide();
        jQuery('#sales-rep-container').fadeIn();

    });
    /* watches country dropdown for a change to trigger state dropdown */
    //#country,
    jQuery('#sales-rep-country, #country').on('change', function() {
        remove_business_card();
        var id = jQuery(this).val();
        switch(id) {
            case 'US':
                jQuery('#state, #sales-rep-state').html(states.US);
                jQuery('#state-dropdown, .sales-rep-state').css('visibility', 'visible');
                jQuery('#state, #sales-rep-state').attr('required', true);
                break;
            case 'CA':
                jQuery('#state, #sales-rep-state').html(states.CA);
                jQuery('#state-dropdown, .sales-rep-state').css('visibility', 'visible');
                jQuery('#state, #sales-rep-state').attr('required', true);
                break;
            default:
                jQuery('#state, #sales-rep-state').html('');
                jQuery('.sales-rep-product').css('visibility', 'visible');
                jQuery('#state-dropdown, .sales-rep-state').css('visibility', 'hidden');
                jQuery('#state, #sales-rep-state').attr('required', false);
        }
    });
    jQuery('#sales-rep-state').on('change', function() {
        remove_business_card();
        jQuery('.sales-rep-product').css('visibility', 'visible');
    })

    function getSalesRepsEmails(product) {
        var emails = [];
        var country = jQuery('#sales-rep-country').val();
        var state = jQuery('#sales-rep-state').val() ? jQuery('#sales-rep-state').val() : country;
        var countryRep = getCountryRep(product, country, state);
        for(var i = 0; i < countryRep.length; i++) {
            var abbrv = countryRep[i];
            if(abbrv) { // removes blank values
                var email = reps[abbrv].email;
                if(jQuery.inArray(email, emails) == -1) {  //prevents duplicate email addresses
                    emails.push(reps[abbrv].email);
                }
            }
        }
        return emails;
    }

    function getSalesRepsData(product) {
        var temp_reps = [];
        var country = jQuery('#sales-rep-country').val();
        var state = jQuery('#sales-rep-state').val() ? jQuery('#sales-rep-state').val() : country;
        var countryRep = getCountryRep(product, country, state);
        for(var i = 0; i < countryRep.length; i++) {
            var abbrv = countryRep[i];
            if(abbrv) { // removes blank values
                var rep = reps[abbrv];
                if(jQuery.inArray(rep, temp_reps) == -1) {  //prevents duplicate email addresses
                    temp_reps.push(rep);
                }
            }
        }
        return temp_reps;
    }

    function getSalesRep() {
        var salesreps = [];
        var emails = [];
        var product = jQuery('#sales-rep-product').val();
        salesreps = getSalesRepsData(product);
        return salesreps;
    }

    function remove_business_card() {
        var bc_exists = jQuery('.business-card').length;
        if(bc_exists) jQuery('.business-card').remove();
    }
    jQuery('#sales-rep-product').on('change', function() {
        remove_business_card();
        var sales_reps = getSalesRep();
        message = displaySalespersonCard(sales_reps);
        jQuery('#sales-rep-content').append(message);
    })

    function displaySalespersonCard(sales_reps) {
        var message = '';
        for(var i = 0; i < sales_reps.length; i++) {
            var person = sales_reps[i];
            message += '<div class="col-md-6 business-card">';
            message += '<div class="col-sm-6" style="margin-top: 3px;">';
            message += '<p><strong>' + person.name + '</strong></p>';
            message += '<p>' + person.title + '</p>';
            message += '<p>phone: <a href="tel:' + person.phone + '">' + person.phone + '</a></p>';
            message += '<p>fax: ' + person.fax + '</p>';
            message += '<p><a href="mailto:' + person.email + '">' + person.email + '</a></p>';
            message += '<p>' + person.address + '</p></div>';
            message += '<div class="col-sm-6" style="padding-right: 0;"><img style="float:right;" src="/wp-content/uploads/2017/04/' + person.picture + '">';
            message += '</div><div class="clear"></div></div>';
        }
        return message;
    }
})
</script>
