<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/styles.css" />
<?php wp_head();
$language = get_language();
$homepage = [
    'en' => '/',
    'es' => '/es/',
    'fr' => '/fr/',
    'pt' => '/pt/',
    'zh' => '/zh/'
];
?>
</head>
<body <?php body_class(); ?>>
<header id="header" role="banner">
    <div class="container">
        <div class="col-sm-6 col-md-4 utility-container">
            <?php wp_nav_menu( [
                'container' => 'nav',
                'container_class' => 'utility-menu',
                'menu' => 'utility-menu'
            ] ); ?>

        </div>
        <div class="col-md-4 col-sm-12 col-md-offset-0 col-xs-10 logo">
            <a href="<?php echo $homepage[$language]; ?>">
                <img src="/wp-content/uploads/2017/04/dsb_logo_color.png" alt="The DS Brown Company">
            </a>
        </div>
        <div class="col-sm-12 col-md-4 utility-container">
            <div id="search" class="col-md-12 col-sm-5">
                <?php get_search_form(); ?>
            </div>
            <div id="language" class="col-md-12 col-sm-3">
                <?php wp_nav_menu( [
                    'container' => 'nav',
                    'container_class' => 'language-menu',
                    'menu' => 'language-menu'
                ] ); ?>
            </div>
            <nav id="social" class="col-md-12 col-sm-4 social-icons">
                <a href="http://visitor.r20.constantcontact.com/manage/optin/ea?v=001IzhgEiH4PbfVb8ahyx7RTH8uonkhqmrgIsfvn8jlXBrFz49bRwZO4VjDKRz8rx3x3QJhenMnNDRcL9isp8SFooUiQ-QaPok7ytfL_IFMkMBo-5ycdjBMxV1XwR1nNN2y3HX9CgHHifIAnYcNs7WN3lvSOlW4eSz-szCAr4ZpnMVgALmx2OPakuFeIv4XiuKy" target="_blank">Sign Up For Our Newsletter</a>
                <ul>
                	<li><a href="http://www.facebook.com/pages/The-DS-Brown-Company/343352612415542" target="_blank"><img src="/wp-content/uploads/2017/04/DSBrown-Facebook_btn.png" alt="Facebook" style="border-width:0px;"></a></li>
                	<li><a href="https://twitter.com/DSBrownCompany" target="_blank"><img src="/wp-content/uploads/2017/04/DSBrown-Twitter_btn.png" alt="Twitter" style="border-width:0px;"></a></li>
                	<li><a href="http://www.youtube.com/user/TheDSBrownCompany" target="_blank"><img src="/wp-content/uploads/2017/04/DSBrown-YouTube_btn.png" alt="Youtube" style="border-width:0px;"></a></li>
                	<li><a href="http://www.linkedin.com/company/d-s-brown-company" target="_blank"><img src="/wp-content/uploads/2017/04/DSBrown-LinkedIn_btn.png" alt="Linkedin" style="border-width:0px;"></a></li>
                </ul>

            </nav>
        </div>
        <div class="col-xs-2 mobile">
            <a id="responsive-menu-button" href="#sidr-main">MENU</a>
        </div>
        <div class="sidr" style="display: none;">
            <div id="mobile-menu">
                <div id="nav-close">
                    <a class="sidr-close btn btn-small">X Close</a>
                </div>
                <div class="mobile-menu-wrapper">
                    <?php wp_nav_menu( [
                        'menu' => 'utility-menu'
                    ] ); ?>
                    <?php wp_nav_menu( [
                        'menu' => 'primary-meny'
                    ] ); ?>
                    <?php wp_nav_menu( [
                        'menu' => 'language-menu'
                    ] ); ?>

                </div>
            </div>
        </div>
        <div class="col-sm-12 large-menu">
            <?php wp_nav_menu( [
                'menu' => 'primary-meny',
                'container' => 'nav',
                'container_class' => 'main-menu'
            ] ); ?>
        </div>
    </div>
</header>
