<?php
$language = get_language();
$footer_headline = [
    'en' => 'BRIDGE THE WORLD WITH LEADING INFRASTRUCTURE SOLUTIONS.',
    'es' => 'TENDEMOS PUENTES AL MUNDO CON SOLUCIONES LIDERES EN INFRAESTRUCTURA',
    'fr' => 'NOUS RELIONS LE MONDE AVEC LES MEILLEURES SOLUTIONS D\'INFRASTRUCTURE',
    'pt' => 'Construindo pontes em todo o mundo com soluções líderes em infraestrutura',
    'zh' => '凭借领先的基础设施，架起沟通世界的桥梁。'
];
$footer_header = [
    'en' => 'COMPREHENSIVE SOLUTIONS',
    'es' => 'SOLUCIONES INTEGRALES',
    'fr' => 'SOLUTIONS COMPLÈTES',
    'pt' => 'SOLUÇÕES ABRANGENTES',
    'zh' => '全面型解决方案'
];
$footer_content = [
    'en' => 'D.S. Brown has the most comprehensive product-line serving the transportation industry. We manufacture solutions for the most challenging infrastructure applications. Our high quality engineered products are available worldwide for new construction and rehabilitation of bridges, highways, airfields, pavements, and parking structures. Please view our <a href="/Resources/DSBrown_Signature_Projectslowres.pdf" target="_blank">Signature Projects Portfolio</a>',
    'es' => 'DS Brown tiene la línea de productos más completa al servicio de la industria del transporte. Somos fabricantes de soluciones para las aplicaciones de infraestructura más difíciles. Nuestros productos de ingeniería de alta calidad están disponibles a nivel mundial para la nueva construcción y rehabilitación de puentes, carreteras, aeropuertos, pavimentos y estacionamientos.',
    'fr' => 'D.S. Brown propose au secteur de transport une famille complète de produits. Nous créons des solutions pour les infrastructures les plus difficiles. Nos produits fabriqués de haute qualité sont disponibles dans le monde entier pour les nouvelles constructions et la réhabilitation des ponts, des routes, des pistes d\'aéroport, des chaussées et des structures de stationnement.',
    'pt' => 'D.S. Brown possui a linha de produtos mais abrangente a serviço do setor de transportes. Fabricamos soluções para as aplicações mais exigentes em termos de infraestrutura. Nossos produtos de engenharia de alta qualidade estão disponíveis em todo o mundo para construções novas e projetos de reabilitação de pontes, rodovias, pistas de aeroportos e estruturas de estacionamento.',
    'zh' => 'DS Brown 公司为交通运输行业提供最丰富全面的产品系列。我们的解决方案可满足最具挑战的基础设施应用。质量上乘的工程产品适用于在全球范围内新建和翻修桥梁、高速公路、机场、道路和停车场结构。'
];
$contact = [
    'en' => 'Telephone: 419.257.3561<br>Fax: 419.257.2200',
    'es' => 'Teléfono: 419.257.3561<br>Fax: 419.257.2200',
    'fr' => 'Téléphone: +01 419.2573561<br>Télécopieur: +01 419.257.2200',
    'pt' => 'Telephone: 419.257.3561<br>Fax: 419.257.2200',
    'zh' => '电话： 419.257.3561<br>传真： 419.257.2200'
];
?>
<footer id="footer" role="contentinfo">
    <div class="container">
        <div class="bridge col-md-12">
            <div class="__left-shadow"></div>
            <div class="__right-shadow"></div>
            <p class="headline"><?php echo $footer_headline[$language]; ?></p>
            <?php //if(is_front_page()) { ?>
                <div class="col-sm-6 about same">
                    <h2><?php echo $footer_header[$language]; ?></h2>
                    <p><?php echo $footer_content[$language]; ?></p>
                </div>
                <div class="col-sm-2 nav same">
                    <?php wp_nav_menu( [
                        'menu' => 'footer',
                        'after' => '<hr>'
                    ] ); ?>

                </div>
                <div class="col-sm-4 address same">
                    <img src="/wp-content/uploads/2017/04/dsb_logo_white.png">
                    <p>300 East Cherry Street <br> North Baltimore, Ohio <br><?php echo $contact[$language]; ?>

        		</div>
                <div class="copyright col-md-12">
                    &copy; <?php echo date('Y'); ?> D.S. Brown
                        <?php wp_nav_menu( [
                        'container' => '',
                        'menu' => 'privacy',
                        'before' => ' | '
                    ] ); ?>
                </div>
            <?php //} else { ?>
                    <style>
                        /*footer {
                            background: transparent url(/wp-content/uploads/2017/04/home-bottom-bkg.png) top center repeat-x;
                            width: 100%;
                            margin-top: -27px;
                        }
                        footer .container {
                            background: url(/wp-content/uploads/2017/04/footer-image.png) top center no-repeat;
                            /*height: 239px;*/
                        } */
                    </style>


            <?php // } ?>
        </div>
    </div>

</footer>

<?php wp_footer(); ?>
<script>
(function(e){var t=!1,i=!1,n={isUrl:function(e){var t=RegExp("^(https?:\\/\\/)?((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|((\\d{1,3}\\.){3}\\d{1,3}))(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*(\\?[;&a-z\\d%_.~+=-]*)?(\\#[-a-z\\d_]*)?$","i");return t.test(e)?!0:!1},loadContent:function(e,t){e.html(t)},addPrefix:function(e){var t=e.attr("id"),i=e.attr("class");"string"==typeof t&&""!==t&&e.attr("id",t.replace(/([A-Za-z0-9_.\-]+)/g,"sidr-id-$1")),"string"==typeof i&&""!==i&&"sidr-inner"!==i&&e.attr("class",i.replace(/([A-Za-z0-9_.\-]+)/g,"sidr-class-$1")),e.removeAttr("style")},execute:function(n,s,a){"function"==typeof s?(a=s,s="sidr"):s||(s="sidr");var r,d,l,c=e("#"+s),u=e(c.data("body")),f=e("html"),p=c.outerWidth(!0),g=c.data("speed"),h=c.data("side"),m=c.data("displace"),v=c.data("onOpen"),y=c.data("onClose"),x="sidr"===s?"sidr-open":"sidr-open "+s+"-open";if("open"===n||"toggle"===n&&!c.is(":visible")){if(c.is(":visible")||t)return;if(i!==!1)return o.close(i,function(){o.open(s)}),void 0;t=!0,"left"===h?(r={left:p+"px"},d={left:"0px"}):(r={right:p+"px"},d={right:"0px"}),u.is("body")&&(l=f.scrollTop(),f.css("overflow-x","hidden").scrollTop(l)),m?u.addClass("sidr-animating").css({width:u.width(),position:"absolute"}).animate(r,g,function(){e(this).addClass(x)}):setTimeout(function(){e(this).addClass(x)},g),c.css("display","block").animate(d,g,function(){t=!1,i=s,"function"==typeof a&&a(s),u.removeClass("sidr-animating")}),v()}else{if(!c.is(":visible")||t)return;t=!0,"left"===h?(r={left:0},d={left:"-"+p+"px"}):(r={right:0},d={right:"-"+p+"px"}),u.is("body")&&(l=f.scrollTop(),f.removeAttr("style").scrollTop(l)),u.addClass("sidr-animating").animate(r,g).removeClass(x),c.animate(d,g,function(){c.removeAttr("style").hide(),u.removeAttr("style"),e("html").removeAttr("style"),t=!1,i=!1,"function"==typeof a&&a(s),u.removeClass("sidr-animating")}),y()}}},o={open:function(e,t){n.execute("open",e,t)},close:function(e,t){n.execute("close",e,t)},toggle:function(e,t){n.execute("toggle",e,t)},toogle:function(e,t){n.execute("toggle",e,t)}};e.sidr=function(t){return o[t]?o[t].apply(this,Array.prototype.slice.call(arguments,1)):"function"!=typeof t&&"string"!=typeof t&&t?(e.error("Method "+t+" does not exist on jQuery.sidr"),void 0):o.toggle.apply(this,arguments)},e.fn.sidr=function(t){var i=e.extend({name:"sidr",speed:200,side:"left",source:null,renaming:!0,body:"body",displace:!0,onOpen:function(){},onClose:function(){}},t),s=i.name,a=e("#"+s);if(0===a.length&&(a=e("<div />").attr("id",s).appendTo(e("body"))),a.addClass("sidr").addClass(i.side).data({speed:i.speed,side:i.side,body:i.body,displace:i.displace,onOpen:i.onOpen,onClose:i.onClose}),"function"==typeof i.source){var r=i.source(s);n.loadContent(a,r)}else if("string"==typeof i.source&&n.isUrl(i.source))e.get(i.source,function(e){n.loadContent(a,e)});else if("string"==typeof i.source){var d="",l=i.source.split(",");if(e.each(l,function(t,i){d+='<div class="sidr-inner">'+e(i).html()+"</div>"}),i.renaming){var c=e("<div />").html(d);c.find("*").each(function(t,i){var o=e(i);n.addPrefix(o)}),d=c.html()}n.loadContent(a,d)}else null!==i.source&&e.error("Invalid Sidr Source");return this.each(function(){var t=e(this),i=t.data("sidr");i||(t.data("sidr",s),"ontouchstart"in document.documentElement?(t.bind("touchstart",function(e){e.originalEvent.touches[0],this.touched=e.timeStamp}),t.bind("touchend",function(e){var t=Math.abs(e.timeStamp-this.touched);200>t&&(e.preventDefault(),o.toggle(s))})):t.click(function(e){e.preventDefault(),o.toggle(s)}))})}})(jQuery);
jQuery(window).load(function() {
    jQuery('#menu-privacy li').eq(0).html(' | ' +jQuery('#menu-privacy li').eq(0).find('a').text());
    jQuery('#responsive-menu-button').sidr({
        name: 'sidr-main',
        source: '#mobile-menu',
        side: 'right'
    });
    jQuery.sidr('close','sidr-main');
    jQuery('.sidr-class-sidr-close').click(function() {
        jQuery.sidr('close','sidr-main');
        return false;
    });
    jQuery('.sidr-class-menu-item-has-children').each(function() {
        jQuery(this).prepend('<span class="sidr-dropdown" alt="open dropdown">v</span>');
    })
    jQuery('.sidr').on('click', '.sidr-dropdown', function(e) {
        e.stopImmediatePropagation();
        jQuery(this).parent().find('> ul').toggle();
    });
    
    jQuery('.resources-content h2:has(a+br), .resources-content h2:has(a+a)').each(function() {
        jQuery(this).find('a').css('font-weight', 700).css('font-size', '12px');

    });


    jQuery('.resources-content > div').each(function() {
        var clear = jQuery(this).css('clear');
        var height = jQuery(this).css('height');
        if(clear == 'both' && height == '20px') {
            jQuery(this).css('padding', 0);
        }
    });
    var content_div = jQuery('.entry-content > div').eq(0).attr('class');
    console.log(content_div);
    var total_divs = jQuery('.entry-content > .'+content_div+' > div').length;
    jQuery('.entry-content > .'+content_div+' > div').each(function(index) {
        if (index === total_divs - 1) {
            if(!jQuery(this).hasClass('clear')) {
                console.log(jQuery(this))
                jQuery(this).after('<div class="clear"></div>');
            }
        }
    });

    /* through all images in content section */
    jQuery('.entry-content img').each(function() {
        var imagename = jQuery(this).attr('src');
        console.log(imagename);
        if(imagename.indexOf('WebButton_WithCart_Green') > -1) {
            jQuery(this).css('border', 'none');
        }
    });




});
</script>
</body>
</html>
