<?php get_header(); ?>
<section id="content" role="main">
<article id="post-0" class="post not-found">
<header class="header">
<h1 class="entry-title"><?php _e( 'Not Found', 'dsbrown' ); ?></h1>
</header>
<section class="entry-content">
<p><?php _e( 'Nothing found for the requested page. Try a search instead?', 'dsbrown' ); ?></p>
<?php get_search_form(); ?>
</section>
</article>
</section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>


<?php get_header(); ?>
<div class="content-wrapper error-404 <?php echo getWebsiteSection(); ?>">
    <section id="content" role="main" class="container content-container" style="background: none;">
        <div class="left-shadow"></div>
        <div class="right-shadow"></div>

            <article id="" <?php post_class('row'); ?> style="padding-top: 30px; background: #C4C0A0;">
                <section class="entry-content col-md-8 col-md-offset-2">
                   <header class="header">
                       <h2 class="entry-title">Sorry, the page you were looking for cannot be found. Would you like to search instead?</h2>
                   </header>

                   <p><?php _e( 'Nothing found for the requested page. Try a search instead?', 'dsbrown' ); ?></p>
                   <?php get_search_form(); ?>
                </section>
            </article>
            <div style="height: 50px"></div>
    </section>
</div>
<?php get_footer(); ?>
